#include <msp430.h>
#include <string.h>
#include<math.h>


#define OK 50

float S=0;
 float T=0;
//***********************************************************************

 //*********************************************************************************
 float calculateDistance1(float signalLevelInDb, float freqInMHz)
 {
     float exp = (25- (20 * 3.382377303) + abs(signalLevelInDb)) / 30.0;
    return pow(10.0, exp);
    // return pow10(exp);
}
 //----------------------------------------------------------------------
 float calculateDistance2(float signalLevelInDb, float freqInMHz)
  {
      float exp = (25- (20 * 3.382377303) + abs(signalLevelInDb)) / 20.0;
     return pow(10.0, exp);
     // return pow10(exp);
 }

 //----------------------------------------------------------------------

 float getTrilateration(float *x,float *y, float x1,float y1, float x2, float y2, float x3, float y3, float r1, float r2, float r3 )
  {

     //*x=1;
    // *y=1;

      S = (pow(x1, 2.) - pow(x2, 2.) + pow(y3, 2.) - pow(y2, 2.) + pow(r2, 2.) - pow(r3, 2.)) / 2.0;
     T = (pow(x1, 2.) - pow(x2, 2.) + pow(y1, 2.) - pow(y2, 2.) + pow(r2, 2.) - pow(r1, 2.)) / 2.0;

     *y = ((T * (x2 - x3)) - (S * (x2 - x1))) / (((y1 - y2) * (x2 - x3)) - ((y3 - y2) * (x2 - x1)));
     *x = ((*y * (y1 - y2)) - T) / (x2 - x1);

     return 1;

 }

 //----------------------------------------------------------------------

 float getSignal(char* rssi)
 {

     return -1*((int)rssi[0]-48+((int)rssi[1]-48)*10);
 }

 //----------------------------------------------------------------------

void Configure_Clock(void)
{
    DCOCTL  = 0; //// Select lowest DCOx and MODx settings<
    BCSCTL1 = CALBC1_16MHZ; // Set DCO
    DCOCTL = CALDCO_16MHZ; //
}

//----------------------------------------------------------------------

void Configure_UART(void)
{

    P1SEL |= BIT1 + BIT2; // SECONDARY PERIPHERAL MODUL FUNCION ->UART
    P1SEL2 |= BIT1 + BIT2; // ........................................

    UCA0CTL1 = UCSWRST;

    UCA0CTL1 |= UCSSEL_2; // SMCLK

    UCA0MCTL = UCBRS_7; // TABLE PAGE 424

    UCA0BR0 = 0x8A; // clock 12mhz + baud rate 115200 -> UCBRx =104=0x68 TABLE PAGE 424
    UCA0BR1 = 0x00; // UCBR1=0

    UCA0CTL1 &=~UCSWRST; // **Initialize USCI state machine**

    IE2 |= UCA0RXIE; // ENABLE RX INTERRUPT

    _BIS_SR(GIE); // global enable interrupt
}

//----------------------------------------------------------------------

int i = 0,h0=0,h1=0,h2=0,j0 = 0,j1=0,j2=0; // CAC BIEN DEM
char* command; // CHUOI CHUA LENH AT COMMAND
int len = 0; // BIEN DO DAI CHUOI CUA LENH AT
float DIS1 =0,DIS2=0,DIS3=0; //KHOANG CACH TU ESP 8266 DEN 1 AP

float RS1 = 0,RS2,RS3; // DO MANH TIN HIEU RSSI
float POSI; //DUNG DE TRA VE VI TRI CUA ROBOT

float x=0; // VI TRI THEO TRUC X
float y=0; // VI TRI THEO TRUC Y

//----------------------------------------------------------------------

void UARTSendString(char* commandz)
{
    i = 0;
    command = commandz;
    len = strlen(commandz);
    IE2 |= UCA0TXIE;

  while((IE2 & UCA0TXIE)==UCA0TXIE){}//check if not set
   //if set, TX interrupt is pending

}
//-------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------

//char APname[3][25]={"Nhan_308", "ThuaWifi", "MYLUA"}; // AP NAME

//char APname[3][25]={"Redmi", "ThuaWifi", "MYLUA"}; // AP NAME

char APname[3][25]={"Embedded System", "ES_02", "ES_03"}; // AP NAME

//char APconnect[20]="AI-THINKER_ACA782";

char RSSI[3][7];

int TEMP=0;

float a=0,b=0;

char charsend[21];
int check=0,num1=0,num2=0, inc=0;
//----------------------------------------------------------------------



void Stat_Stop();
void Stat_Forward();
void Stat_TurnLeft();
void Stat_TurnRight();

void main(void)
{
    WDTCTL = WDTPW + WDTHOLD;           // Stop Watchdog

   // DCOCTL  = 0; //// Select lowest DCOx and MODx settings<
     //  BCSCTL1 = CALBC1_16MHZ; // Set DCO
       //DCOCTL = CALDCO_16MHZ; //
   // P1DIR  |=  BIT0 + BIT6;             // P1.0-TXLED and P1.6-RXLED output
    //P1OUT  &= ~BIT0 + BIT6;             // P1.0 and P1.6 = 0

    Configure_Clock();
    Configure_UART();

    P1DIR|= BIT0+BIT6;
    P1OUT=0x00;

        UARTSendString("AT+RST\r\n");//
        __delay_cycles(3500000);


        UARTSendString("AT+CWMODE=1\r\n");
        __delay_cycles(10000000);
        UARTSendString("AT+CWLAP\r\n");//UARTSendString("AT+CWLAP=\"Nhan_308\"\r\n");
             __delay_cycles(35000000);



     /*   UARTSendString("AT+CWJAP=\"ES_03\",\"59605287\"\r\n"); // connect to an AP with  password "_"
          __delay_cycles(100000000);
          P1OUT|= BIT0;

            UARTSendString("AT+CIPSTART=\"TCP\",\"192.168.0.100\",9999\r\n");

           __delay_cycles(70000000);
           P1OUT|=BIT6;

           UARTSendString("AT+CIPSEND=8\r\n");
          __delay_cycles(4500000);


            UARTSendString("Nhan,1,2\r\n");
            __delay_cycles(1000000);*/

         P1OUT &=0x00;

         float RS1R=0,RS2R=0,RS3R=0;






     _BIS_SR(GIE);



     while(1)
    {

         UARTSendString("AT+CWLAP\r\n");//UARTSendString("AT+CWLAP=\"Nhan_308\"\r\n");
                     __delay_cycles(35000000);

                     RS1 =getSignal(RSSI[0]);
                     RS2 = getSignal(RSSI[1]);
                     RS3=getSignal(RSSI[2]);




                     if(inc >3)
                     {
                         Stat_TurnRight();
                         __delay_cycles(3000000);
                         Stat_Forward();
                         __delay_cycles(18000000);

                         Stat_Stop();
                         __delay_cycles(1000000);
                         inc =0;
                     }

                     if(RS1 < 30)
                     {
                         P1OUT|=BIT0;
                         P1OUT&=~BIT6;
                         Stat_Stop();

                     }
                     else if(RS1<RS1R && RS1<RS2 && RS1<RS3)
                     {
                         RS1R=RS1;
                                                  RS2R=RS2;
                                                  RS3R=RS3;

                         P1OUT|=BIT6;
                         P1OUT&=~BIT0;
                         Stat_Forward();
                          __delay_cycles(18000000);
                         Stat_Stop();
                     }
                     else if (RS1>RS1R||RS1>RS2 || RS1>RS3)
                     {
                         RS1R=RS1;
                         RS2R=RS2;
                         RS3R=RS3;

                         inc++;
                         P1OUT|=BIT6;
                         P1OUT&=~BIT0;
                         Stat_TurnLeft();
                         __delay_cycles(2700000);
                         Stat_Stop();
                         __delay_cycles(1000000);
                         Stat_Forward();
                         __delay_cycles(18000000);
                         Stat_Stop();

                     }

                     else if( RS1>RS1R && RS2<RS2R && RS3>RS3R)// tien gan AP thu 2
                     {
                         RS1R=RS1;
                         RS2R=RS2;
                         RS3R=RS3;

                         P1OUT|=BIT6;
                         P1OUT&=~BIT0;

                         Stat_TurnRight();
                         __delay_cycles(3000000);
                         Stat_Stop();
                         __delay_cycles(1000000);
                         Stat_Forward();
                         __delay_cycles(18000000);
                          Stat_Stop();
                     }
                     else if( RS1<RS1R && RS2>RS2R && RS3>RS3R)// tien gan AP thu 1
                     {
                         RS1R=RS1;
                         RS2R=RS2;
                                                  RS3R=RS3;

                                                  P1OUT|=BIT6;
                                                  P1OUT |=BIT0;

                         Stat_Forward();
                         __delay_cycles(18000000);
                         Stat_Stop();
                     }

                     else if( RS1<RS1R && RS2>RS2R && RS3<RS3R)// tien gan AP thu 3
                     {
                         RS1R=RS1;
                                                  RS2R=RS2;
                                                  RS3R=RS3;

                         P1OUT|=BIT6;
                         P1OUT&=~BIT0;

                         inc++;
                         Stat_TurnLeft();
                          __delay_cycles(3000000);
                          Stat_Stop();
                         __delay_cycles(1000000);
                         Stat_Forward();
                         __delay_cycles(18000000);
                          Stat_Stop();
                     }



                     else{

                         if(RS1>RS1R && num1<=3)// chay xa khoi AP1 1
                         {
                             if(num2 >2)
                                   num2=0;

                             P1OUT|=BIT6;
                             P1OUT&=~BIT0;

                             inc++;
                             RS1R=RS1;
                             num1++;
                             Stat_TurnLeft();
                             __delay_cycles(3000000);
                             Stat_Forward();
                              __delay_cycles(18000000);
                               Stat_Stop();


                         }

                         else if(RS1>RS1R && 3<num1<=4 )// chay xa khoi AP1  2
                        {
                             if(num2 >2)
                                 num2=0;
                             P1OUT|=BIT6;
                                 P1OUT&=~BIT0;
                                 RS1R=RS1;

                             Stat_TurnRight();
                              __delay_cycles(3000000);
                               Stat_Forward();
                                __delay_cycles(18000000);
                               Stat_Stop();
                                num1++;

                          }
                         else if(RS1<RS1R && num2<3) //chay gan toi AP1
                         {

                             RS1R=RS1;
                             P1OUT|=BIT6;
                             P1OUT&=~BIT0;
                             Stat_Forward();
                              __delay_cycles(18000000);
                              Stat_Stop();
                              num2++;
                              num1=0;

                         }
                         else if (num1>4)
                             num1=0;

                         P1OUT|=BIT6;
                         P1OUT&=~BIT0;

                        // Stat_TurnRight();
                       //  __delay_cycles(4000000);

                         Stat_Stop();

                     }

                     /**/



    }
     return 0;
}



void Stat_Stop()
{
    P2DIR |=BIT0 +BIT1 +BIT3 +BIT4;
    P2OUT &= ~(BIT0 +BIT1 +BIT3 +BIT4);
}
void Stat_Forward()
{
    P2DIR |=BIT0 +BIT1 +BIT3 +BIT4;
    //Banh A
    P2OUT |=BIT0;
    P2OUT &= ~BIT1;
    //Banh B
    P2OUT |=BIT3;
    P2OUT &= ~BIT4;
}
void Stat_TurnLeft()
{
    P2DIR |=BIT0 +BIT1 +BIT3 +BIT4;
    P2OUT |=BIT0;
    P2OUT &= ~BIT1;
        //Banh B
    P2OUT &=~BIT3;
    P2OUT &=~BIT4;
}
void Stat_TurnRight()
{
    P2DIR |=BIT0 +BIT1 +BIT3 +BIT4;
    //Banh A
    P2OUT &=~BIT0;
    P2OUT |=BIT1;
    //Banh B
    P2OUT |=BIT3;
    P2OUT &= ~BIT4;

}

#pragma vector = USCIAB0TX_VECTOR
__interrupt void TraUART(void)
{

    if(i >= len-1)
    {
        IE2 &= ~UCA0TXIE; //neu da het du lieu can truyen thi tat che do truyen
    }

    UCA0TXBUF = command[i++]; // truyen di ki tu thu i cua lenh AT command
}

//----------------------------------------------------------------------

#pragma vector = USCIAB0RX_VECTOR
__interrupt void RecUART(void)
{

    if(h0 >= 0)
    {
        RSSI[0][h0] = UCA0RXBUF; // GAN GIA TRI CUA BUFFER RX VAO RSSI DE LAY DO MANH CUA TIN HIEU
        h0--;
    }

    if(h1 >= 0)
        {
            RSSI[1][h1] = UCA0RXBUF; // GAN GIA TRI CUA BUFFER RX VAO RSSI DE LAY DO MANH CUA TIN HIEU
            h1--;
        }

    if(h2 >= 0)
            {
                RSSI[2][h2] = UCA0RXBUF; // GAN GIA TRI CUA BUFFER RX VAO RSSI DE LAY DO MANH CUA TIN HIEU
                h2--;
            }


    // DO TIM TEN CUA AP CAN LAY RSSI

    if(UCA0RXBUF == APname[0][j0]) // NEU TIM THAY
    {
        j0++;

    }
    else // NEU K TIM THAY
    {
        j0= 0;
    }

    if(UCA0RXBUF == APname[1][j1]) // NEU TIM THAY
        {
            j1++;

        }
        else // NEU K TIM THAY
        {
            j1= 0;
        }


    if(UCA0RXBUF == APname[2][j2]) // NEU TIM THAY
        {
            j2++;

        }
        else // NEU K TIM THAY
        {
            j2= 0;
        }

    //------------------------------
    if(j0 == strlen(APname[0]))// NEU SO LUONG KI TU CUA TU TIM THAY TRUNG VOI DO DAI CUA TEN AP CAN TIM
    {
        h0 =4;
    }

    if(j1 == strlen(APname[1]))// NEU SO LUONG KI TU CUA TU TIM THAY TRUNG VOI DO DAI CUA TEN AP CAN TIM
        {
            h1 =4;
        }

    if(j2 == strlen(APname[2]))// NEU SO LUONG KI TU CUA TU TIM THAY TRUNG VOI DO DAI CUA TEN AP CAN TIM
        {
            h2 =4;
        }

}
